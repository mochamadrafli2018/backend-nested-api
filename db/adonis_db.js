const adonis = [
  {
    "id": 1,
    "title": "Adonis #1",
    "review": "Sumber referensi cara instalasi dan set up laravel dapat dicermati pada sumber referensi berikut.",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": true,
    "accordion": 
    [
      {
        "eventKey":1,
        "item":"Pengenalan dan Instalasi Adonis",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"Set up Adonis",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":3,
        "item":"Struktur direktori Adonis",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      }
    ]
  },  
  {
    "id": 2,
    "title": "Adonis #2",
    "review": "Sumber referensi cara migrasi database, memahami konsep MVC, serta membuat model, view dan controller dapat dicermati pada sumber referensi berikut.",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": true,
    "accordion": 
    [
      {
        "eventKey":1,
        "item":"Konsep MVC dan template engine Adonis",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"Membuat Model, View, Controller Adonis dan Routing",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      }
    ]
  },
  {
    "id": 3,
    "title": "Adonis #3",
    "review": "Sumber referensi cara membuat REST-API dengan Laravel dapat dicermati pada sumber referensi berikut.",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": true,
    "accordion": 
    [
      {
        "eventKey":1,
        "item":"GET Method",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"POST Method",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":3,
        "item":"PUT Method",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":4,
        "item":"DELETE Method",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      }
    ]
  }  
];


module.exports = adonis;
