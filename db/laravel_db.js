const laravel = [
  {
    "id": 1,
    "title": "Laravel #1",
    "review": "Sumber referensi cara instalasi dan set up laravel",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": true,
    "accordion": 
    [
      {
        "eventKey":1,
        "item":"Instalasi XAMPP",
        "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-3",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"Instalasi Composer",
        "link":"https://www.niagahoster.co.id/blog/cara-install-composer/",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":3,
        "item":"Instalasi Laravel",
        "link":"https://www.niagahoster.co.id/blog/cara-install-laravel-di-windows/",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      }
    ]
  },
  {
    "id": 2,
    "title": "Laravel #2",
    "review": "Sumber referensi mengenai struktur direktori dan artisan",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": true,
    "accordion": 
    [
      {
        "eventKey":1,
        "item":"Struktur Direktori Laravel",
        "link":"https://medium.com/easyread/struktur-folder-laravel-framework-299f0225cd55",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"Artisan CLI (Command Line Interface)",
        "link":"https://medium.com/easyread/apa-itu-artisan-cli-pada-laravel-62a94232a29a",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      }
    ]
  },
  {
    "id": 3,
    "title": "Laravel #3",
    "review": "Sumber referensi untuk memulai migrasi database, memahami konsep MVC, serta membuat model, view dan controller",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": true,
    "accordion": 
    [
      {
        "eventKey":1,
        "item":"Migrasi Database",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"Konsep MVC (Model, View, Controller)",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":3,
        "item":"Routing API dan Web",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      }
    ]
  },
  {
    "id": 4,
    "title": "Laravel #4",
    "review": "Sumber referensi cara membuat REST API sederhana dengan HTTP Verb (GET, POST, PUT, DELETE)",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": false,
    "accordion": 
    [
      {
        "eventKey":1,
        "item":"GET Method",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"POST Method",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":3,
        "item":"PUT Method",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":4,
        "item":"DELETE Method",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      }
    ]
  },
  {
    "id": 5,
    "title": "Laravel #5",
    "review": "Sumber referensi cara membuat CRUD di Laravel dengan memanfaatkan template engine blade",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": false,
    "accordion":  
    [
      {
        "eventKey":1,
        "item":"Crete",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"Read",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":3,
        "item":"Update",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":4,
        "item":"Delete",
        "link":"Lorem ipsum dolor sit amet.",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      }
    ]
  }
];

module.exports = laravel;
