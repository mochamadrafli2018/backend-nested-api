const adonis = require('../db/adonis_db');
const laravel = require('../db/laravel_db');
const react = require('../db/react_db');
const getAllDataLaravel = () => ({laravel})
const getAllDataAdonis = () => ({adonis})
const getAllDataReact = () => ({react})

module.exports = {
  getAllDataLaravel, 
  getAllDataAdonis,
  getAllDataReact
};
