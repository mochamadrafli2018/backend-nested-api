const {
  getAllDataLaravel, 
  getAllDataAdonis, 
  getAllDataReact
} = require('./handlerGet')

const routes = [
  {
    method: 'GET',
    path: '/laravel',
    handler: getAllDataLaravel,
  },
  {
    method: 'GET',
    path: '/adonis',
    handler: getAllDataAdonis,
  },
  {
    method: 'GET',
    path: '/react',
    handler: getAllDataReact,
  },
];
module.exports = routes;
